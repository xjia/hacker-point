import webapp2
import jinja2
from webapp2_extras import sessions
from google.appengine.ext import ndb

import os
import json
import logging
import urlparse

import sanction
import config
import secret
import score
from models import GitHubUser

jinja_environment = jinja2.Environment(loader = jinja2.FileSystemLoader(os.path.dirname(__file__)))

class BaseHandler(webapp2.RequestHandler):
  def dispatch(self):
    self.session_store = sessions.get_store(request = self.request)
    try:
      webapp2.RequestHandler.dispatch(self)
    finally:
      self.session_store.save_sessions(self.response)
  
  @webapp2.cached_property
  def session(self):
    return self.session_store.get_session()

  def render(self, html, template_values = {}):
    template = jinja_environment.get_template(html)
    self.response.out.write(template.render(template_values))

class HomeHandler(BaseHandler):
  def get(self):
    self.render('home.html', {
      'name'  : self.session.get('name'),
      'score' : self.session.get('score')
    })

class GitHubLoginHandler(BaseHandler):
  def get(self):
    c = sanction.Client(auth_endpoint = 'https://github.com/login/oauth/authorize',
                        client_id     = config.GITHUB_CLIENT_ID,
                        redirect_uri  = config.GITHUB_REDIRECT_URI)
    self.redirect(c.auth_uri())

class GitHubCallbackHandler(BaseHandler):
  def get(self):
    c = sanction.Client(token_endpoint    = 'https://github.com/login/oauth/access_token',
                        resource_endpoint = 'https://api.github.com',
                        redirect_uri      = config.GITHUB_REDIRECT_URI,
                        client_id         = config.GITHUB_CLIENT_ID,
                        client_secret     = secret.GITHUB_CLIENT_SECRET)
    c.request_token(code = self.request.get('code'),
                    parser = lambda data: dict(urlparse.parse_qsl(data)))
    data = c.request('/user')
    self.__login(data)
    self.redirect('/')

  def __login(self, data):
    key = ndb.Key(GitHubUser, data['id'])
    ent = key.get()
    
    if ent is None:
      ent = GitHubUser(key = key,
                       email = data['email'],
                       login = data['login'])
      ent.score = score.GitHub(login = ent.login, user_data = data).get()
      ent.put()
    elif ent.email != data['email'] or ent.login != data['login']:
      ent.email = data['email']
      ent.login = data['login']
      ent.put()
    
    self.session['name'] = data['name']
    self.session['score'] = ent.score

class ConnectStackExchangeHandler(BaseHandler):
  def get(self):
    c = sanction.Client(auth_endpoint = 'https://stackexchange.com/oauth',
                        client_id     = config.STACKEXCHANGE_CLIENT_ID,
                        redirect_uri  = config.STACKEXCHANGE_REDIRECT_URI)
    self.redirect(c.auth_uri())

class StackExchangeCallbackHandler(BaseHandler):
  def get(self):
    c = sanction.Client(token_endpoint    = 'https://stackexchange.com/oauth/access_token',
                        resource_endpoint = 'https://api.stackexchange.com/2.1',
                        redirect_uri      = config.STACKEXCHANGE_REDIRECT_URI,
                        client_id         = config.STACKEXCHANGE_CLIENT_ID,
                        client_secret     = secret.STACKEXCHANGE_CLIENT_SECRET)
    c.request_token(code = self.request.get('code'),
                    parser = lambda data: dict(urlparse.parse_qsl(data)))
    data = c.request('/me/associated', query = { 'key': config.STACKEXCHANGE_KEY })
    self.__connect(data)
#   self.redirect('/')
  
  def __connect(self, data):
    reputation = 0
    for item in data['items']:
      if item['reputation'] > 10:
        reputation += item['reputation']
    self.response.write("reputation = %d" % reputation)

class ConnectHandler(BaseHandler):
  def show_instructions(self, site):
    self.render("connect-%s.html" % site)

class ConnectTopCoderHandler(ConnectHandler):
  def get(self):
    self.show_instructions('topcoder')

class ConnectSpojHandler(ConnectHandler):
  def get(self):
    self.show_instructions('spoj')

class ConnectSguHandler(ConnectHandler):
  def get(self):
    self.show_instructions('sgu')

class ConnectPojHandler(ConnectHandler):
  def get(self):
    self.show_instructions('poj')

class RankHandler(BaseHandler):
  def get(self):
    self.response.write('TODO')

app_config = {}
app_config['webapp2_extras.sessions'] = {
  'secret_key': secret.SESSIONS_SECRET_KEY
}

app = webapp2.WSGIApplication([
  (r'/',                        HomeHandler),
  (r'/github/login',            GitHubLoginHandler),
  (r'/github/callback',         GitHubCallbackHandler),
  (r'/connect/stackexchange',   ConnectStackExchangeHandler),
  (r'/stackexchange/callback',  StackExchangeCallbackHandler),
  (r'/connect/topcoder',        ConnectTopCoderHandler),
  (r'/connect/spoj',            ConnectSpojHandler),
  (r'/connect/sgu',             ConnectSguHandler),
  (r'/connect/poj',             ConnectPojHandler),
  (r'/rank',                    RankHandler)
], config = app_config)
