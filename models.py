from google.appengine.ext import ndb

class GitHubUser(ndb.Model):
  email = ndb.StringProperty(required = True)
  login = ndb.StringProperty(indexed = True, required = True)
  score = ndb.IntegerProperty(indexed = True, required = True)
  last_updated = ndb.DateTimeProperty(indexed = True, auto_now = True)
