import json
import logging
from urllib2 import urlopen

class GitHub:
  def __init__(self, login = None, user_data = None, repos_data = None):
    if user_data is None:
      user_data = json.loads(urlopen("https://api.github.com/users/%s" % login).read())
    
    if repos_data is None:
      logging.info("https://api.github.com/users/%s/repos" % login)
      repos_data = json.loads(urlopen("https://api.github.com/users/%s/repos" % login).read())
    
    self.score = self.calculate(user_data, repos_data)
  
  def get(self):
    return self.score
  
  def calculate(self, user_data, repos_data):
    followers = user_data['followers']
    stars = 0
    forks = 0
    
    for repo_data in repos_data:
      if repo_data['watchers'] > 5:
        stars += repo_data['watchers']
      if not repo_data['fork']:
        forks += repo_data['forks']
    
    return followers + stars + forks
